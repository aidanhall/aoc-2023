(defun read-file-matrix (f)
  (let ((ss (uiop:read-file-lines f)))
    (make-array
     (list (length ss) (length (first ss)))
     :element-type 'character
     :initial-contents ss)))
