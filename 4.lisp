;; Part 1
(with-open-file (f "4.txt")
  (loop until (eq 'eof (peek-char t f nil 'eof))
        for winners
          =
          (progn
            (loop until (char= (read-char f) #\:))
            (length
             (intersection
              (loop until (char= #\| (peek-char t f)) collect (read f) finally (read-char f))
              (loop until (char= #\C (peek-char t f nil #\C)) collect (read f)))))
        if (> winners 0)
          sum (expt 2 (1- winners))))

;; Part 2
(with-open-file (f "4.txt")
  (loop until (eq 'eof (peek-char t f nil 'eof))
        for copies = nil
          then (remove-if (lambda (n) (<= n 0))
                          (mapcar #'1- copies))
        
        for winners
          =
          (length
           (progn
             (loop until (char= (read-char f) #\:))
             (intersection
              (loop until (char= #\| (peek-char t f)) collect (read f) finally (read-char f))
              (loop until (char= #\C (peek-char t f nil #\C)) collect (read f)))))
          
        sum (1+ (length copies))
        
        if (> winners 0)
          do (loop for j below (1+ (length copies))
                   do (push (1+ winners) copies))))
