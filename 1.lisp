(defvar digit-names
  '(("one" . "1")
    ("two" . "2")
    ("three" . "3")
    ("four" . "4")
    ("five" . "5")
    ("six" . "6")
    ("seven" . "7")
    ("eight" . "8")
    ("nine" . "9")))
;;; Part 1
(apply
 #'+
 (mapcar
  (lambda (line)
    (let ((chars
            (remove-if-not #'digit-char-p
                           (coerce line 'list))))
      ;; (car (last)) = (first) if only one digit
      (read-from-string
       (concatenate 'string
                    (list (first chars))
                    (last chars)))))
  (uiop:read-file-lines "1.txt")))
;;; Part 2
(apply
 #'+
 (mapcar
  (lambda (line)
    (let ((chars
            (remove-if
             #'null
             (loop for i from 0
                   for c in (coerce line 'list)
                   collect
                   (if (digit-char-p c)
                       (string c)
                       (cdr (assoc
                             (subseq line i)
                             digit-names
                             :test
                             (lambda (a b)
                               (uiop:string-prefix-p b a)))))))))
      (read-from-string
       (concatenate 'string
                    (first chars)
                    (car (last chars))))))
  (uiop:read-file-lines "1.txt")))
