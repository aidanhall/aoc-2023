(load "util.lisp")
(defun symbol? (c)
  (not (or (digit-char-p c) (eql #\. c))))
(loop for y from 0 below (array-dimension i 0)
      collect
      (loop for x from 0 below (array-dimension i 1)
            for item = (aref i x y)
            if (digit-char-p item)
              collect item))
(defun symbol-about? (i y x ymax)
  (loop for y from (max 0 (1- y)) to (min ymax (1+ y))
        if (symbol? (aref i y x))
          return t))
;; Part 1
(let ((i (read-file-matrix "3.txt")))
 (loop
   with ymax = (1- (array-dimension i 0))
   for y from 0 to ymax
   sum
   (loop with xmax = (1- (array-dimension i 1))
         for x from 0 to xmax
         for item = (aref i y x)
         if (and (digit-char-p item)
                 (or (= x 0)
                     (not (digit-char-p (aref i y (1- x))))))
           sum
           (loop
             for x2 from x below (array-dimension i 1)
             for adj =
                     (and (> x 0)
                          (or (symbol-about? i y (1- x) ymax)
                              (symbol-about? i y x ymax)))
               then (or adj (symbol-about? i y x2 ymax))
             while (digit-char-p (aref i y x2))
             collect (aref i y x2) into digits
             finally
                (return
                  (if (or adj (and (< x xmax) (symbol-about? i y (1+ x) ymax)))
                      (parse-integer (coerce digits 'string))
                      0))))))
;; Part 2
(defun number-backward (i y xstart)
  (loop for x from xstart downto 0
        for item = (aref i y x)
        while (digit-char-p item)
        collect item into digits
        finally (return (nreverse digits))))
(defun number-forward (i y xstart)
  (loop for x from xstart below (array-dimension i 1)
        for item = (aref i y x)
        while (digit-char-p item)
        collect item))
(apply
 #'+
 (mapcar
  (lambda (gear)
    (if (= 2 (length gear))
        (apply #'* gear)
        0))
  (let ((i (read-file-matrix "3.txt")))
    (loop
      with ymax = (1- (array-dimension i 0))
      for y from 0 to ymax
      if
      (loop with xmax = (1- (array-dimension i 1))
            for x from 0 to xmax
            for item = (aref i y x)
            if (char= item #\*)
              if
              (mapcar
               (lambda (digits)
                 (parse-integer (coerce digits 'string)))
               (nconc
                (if (> y 0)
                    (let ((above (aref i (1- y) x))
                          (tl (if (> x 0) (number-backward i (1- y) (1- x))))
                          (tr (if (< x xmax) (number-forward i (1- y) (1+ x)))))
                      (if tl
                          (if tr
                              (if (digit-char-p above)
                                  (list (nconc tl (list above) tr))
                                  (list tl tr))
                              (list (nconc tl (if (digit-char-p above)
                                                  (list above)))))
                          (if tr
                              (if (digit-char-p above)
                                  (list (nconc (list above) tr))
                                  (list tr))
                              (if (digit-char-p above)
                                  (list (list above)))))))
                (if (< y ymax)
                    (let ((below (aref i (1+ y) x))
                          (bl (if (> x 0) (number-backward i (1+ y) (1- x))))
                          (br (if (< x xmax) (number-forward i (1+ y) (1+ x)))))
                      (if bl
                          (if br
                              (if (digit-char-p below)
                                  (list (nconc bl (list below) br))
                                  (list bl br))
                              (list (nconc bl (if (digit-char-p below)
                                                  (list below)))))
                          (if br
                              (if (digit-char-p below)
                                  (list (nconc (list below) br))
                                  (list br))
                              (if (digit-char-p below)
                                  (list (list below)))))))
                ))
            collect it)
      append it))))

(defun row-nums-about (i y x xmax)
  (let ((mid (aref i y x))
        (tl (if (> x 0) (number-backward i y (1- x))))
        (tr (if (< x xmax) (number-forward i y (1+ x)))))
    (if tl
        (if tr
            (if (digit-char-p mid)
                (list (nconc tl (list mid) tr))
                (list tl tr))
            (list (nconc tl (if (digit-char-p mid)
                                (list mid)))))
        (if tr
            (if (digit-char-p mid)
                (list (nconc (list mid) tr))
                (list tr))
            (if (digit-char-p mid)
                (list (list mid)))))))

(apply
 #'+
 (mapcar
  (lambda (gear)
    (if (= 2 (length gear))
        (apply #'* gear)
        0))
  (let ((i (read-file-matrix "3.txt")))
    (loop
      with ymax = (1- (array-dimension i 0))
      for y from 0 to ymax
      if
      (loop with xmax = (1- (array-dimension i 1))
            for x from 0 to xmax
            for item = (aref i y x)
            if (char= item #\*)
              if
              (mapcar
               (lambda (digits)
                 (parse-integer (coerce digits 'string)))
               (nconc
                (if (> y 0)
                    (row-nums-about i (1- y) x xmax))
                (lol:aif (and (> x 0) (number-backward i y (1- x)))
                         (list lol:it))
                (lol:aif (and (< x xmax) (number-forward i y (1+ x)))
                         (list lol:it))
                (if (< y ymax)
                    (row-nums-about i (1+ y) x xmax))
                ))
            collect it)
      append it))))
