; Part 1
(loop
  for game-number from 1
  for game in
           (mapcar
            (lambda (l)
              (uiop:split-string
               (subseq l (1+ (search ":" l)))
               :separator '(#\;)))
            (uiop:read-file-lines "2.txt"))
  for (red green blue)
    = (loop for round in game
            for cubes
              = (loop
                  for cube in (uiop:split-string
                               round
                               :separator '(#\,))
                  for num = (parse-integer cube :junk-allowed t)
                  if (search "red" cube)
                    sum num into red
                  if (search "green" cube)
                    sum num into green
                  if (search "blue" cube)
                    sum num into blue
                  finally (return (list red green blue)))
            maximize (first cubes) into red
            maximize (second cubes) into green
            maximize (third cubes) into blue
            finally (return (list red green blue)))
  sum
  (or (and (<= red 12) (<= green 13) (<= blue 14)
           game-number)
      0))

;; Part 2
(loop
  for game in
           (mapcar
            (lambda (l)
              (uiop:split-string
               (subseq l (1+ (search ":" l)))
               :separator '(#\;)))
            (uiop:read-file-lines "2.txt"))
  for (red green blue)
    = (loop for round in game
            for cubes
              = (loop
                  for cube in (uiop:split-string
                               round
                               :separator '(#\,))
                  for num = (parse-integer cube :junk-allowed t)
                  if (search "red" cube)
                    sum num into red
                  if (search "green" cube)
                    sum num into green
                  if (search "blue" cube)
                    sum num into blue
                  finally (return (list red green blue)))
            maximize (first cubes) into red
            maximize (second cubes) into green
            maximize (third cubes) into blue
            finally (return (list red green blue)))
  collect (funcall #'* red green blue))
